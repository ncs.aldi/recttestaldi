<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
	public function Get_user_role()
	{
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->join('user_role', 'user_role.id_user=user.id_user', 'left');
        $this->db->join('role', 'role.id_role=user_role.id_role', 'left');
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	
	
}
