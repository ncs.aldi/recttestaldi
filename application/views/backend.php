<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Selamat datang</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.19.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}

	</style>
</head>

<body>

	<div id="container">
		<div class="row mt-4 p-3">
			<div class="col-md-6">
				<h3 class="text-warning">Tugas 3</h3>

				Untuk Tugas Selanjutnya silanhkan membuat <b>CRUD</b> pada tabel User dengan beberapa ketentuan berikut
				:
				<ul>
					<li style="list-style: decimal;">Menggunakan <b>CI - Query Builder / Laravel Eloquent</b></li>
					<li style="list-style: decimal;">Menggunakan Ajax menjadi nilai tambah</li>
					<li style="list-style: decimal;">Menggunakan <b>Datatables</b></li>
					<li style="list-style: decimal;">Untuk hapus data dan simpan, harus ada konfirmasi lebih dulu </li>
				</ul>
				dengan mekanisme dibawah:
				<ul>
					<li style="list-style: decimal;">Buat <b>controller</b> baru untuk fungsi CRUD yang akan dibuat</li>
					<li style="list-style: decimal;">Buat form dengan view baru atau bisa dengan menggunakan modal</li>
					<li style="list-style: decimal;">Selebihnya silahkan improvisasi sendiri</li>
				</ul>
			</div>
		</div>
		<div class="col-12">
			<div class="card">
				<div class="card-header">Tabel User</div>
				<div class="card-body">
					<button class="btn btn-primary" onclick="add_new()">Add New User</button>
					<table id="user" class="table table-bordered table-hover table-sm" data-show-refresh="true"
						data-show-export="true" data-pagination="true" data-id-field="id" data-page-size=10
						data-page-list="[10, 25, 50, 100, ALL]" data-sort-name="updated_at" data-sort-order="desc"
						data-side-pagination="server">
					</table>
				</div>
			</div>
		</div>

	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds.
		<?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
	</p>
	</div>
	<div class="modal fade" id="mod-add" tabindex="-1" role="dialog" aria-labelledby="modal-proses" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-proses"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body bg-lighter" id="body-progres">
					<form id="form-add">
						<label for="">Nama</label>
						<input type="text" class="form-control" name="nama" required>
						<label for="">Username</label>
						<input type="text" class="form-control" name="username" required>
						<label for="">Password</label>
						<input type="password" class="form-control" name="password" required>
						<label for="">Role</label>
						<select name="role" id="role" class="form-control" required>
							<option value="">-pilih-</option>
							<?php $i = 1;foreach ($role as $r): ?>
							<option value="<?= $r->id_role ?>"><?= $r->nama_role ?></option>
							<?php $i++;endforeach ?>
						</select>
						<input type="submit" id="btnSave" class="btn btn-sm btn-primary btn-block" value="Simpan">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="mod-edit" tabindex="-1" role="dialog" aria-labelledby="modal-proses" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-proses"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body bg-lighter" id="body-progres">
					<form id="form-edit">
						<input type="hidden" id="id_user" value="" name="id">
						<label for="">Nama</label>
						<input type="text" class="form-control" id="nama" name="nama" required>
						<label for="">Username</label>
						<input type="text" class="form-control" id="username" name="username" required>
						<label for="">Password</label>
						<input type="password" class="form-control" name="password" required>
						<label for="">Role</label>
						<select name="role" id="role" class="form-control" required>
							<option value="">-pilih-</option>
							<?php $i = 1;foreach ($role as $r): ?>
							<option value="<?= $r->id_role ?>"><?= $r->nama_role ?></option>
							<?php $i++;endforeach ?>
						</select>
						<input type="submit" id="btnEdit" class="btn btn-sm btn-primary btn-block" value="Simpan">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

</body>

<footer>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
	</script>
	<script src="https://unpkg.com/bootstrap-table@1.19.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script>
		var SITE_URL = '<?= base_url() ?>'
		$(function () {
			$('#user').bootstrapTable({
				search: true,
				url: SITE_URL + 'backend/list_user',
				pageSize: 10,
				pageList: "[5, 10, 20, 50, 100, 200]",
				columns: [{
					field: 'id_user',
					title: 'ID',
					halign: 'center',
					sortable: true
				}, {
					field: 'nama',
					title: 'Nama',
					halign: 'center',
					sortable: true
				}, {
					field: 'nama_role',
					title: 'Role',
					halign: 'center',
					sortable: true
				}, {
					halign: 'center',
					title: 'Action',
					sortable: false,
					formatter: function (value, row, index) {
						return '<button class=\'btn btn-warning btn-sm \' onclick=\'edit(' +row.id_user + ')\'>Edit</button><button class=\'btn btn-danger btn-sm \' onclick=\'hapus(' +row.id_user + ')\'>Hapus</button>';
					}

				}]
			});
		});

		function add_new() {
			$('#mod-add').modal('show');
			$('.modal-title').text('Add Baru');
		}

		function hapus(id) {
			bootbox.confirm("Are you sure?", function (result) {
				if (result) {
					$.ajax({
						url: SITE_URL + 'backend/del_user/' + id,
						type: 'get',
						success: function (res) {
							if (res.status) {
								toastr.success('Data Sukses Dihapus');
								$('#user').bootstrapTable('refresh');
							} else {
								toastr.error('Data Gagal Dihapus')
							}
						}
					});
				}
			})
		}
		$('#form-add').submit(function (event) {
			event.preventDefault();
			$('#btnSave').text('saving...');
			$('#btnSave').attr('disabled', true);
			bootbox.confirm("Are you sure?", function (result) {
			$.ajax({
				url: SITE_URL + 'backend/add_new',
				type: "POST",
				data: $('#form-add').serialize(),
				dataType: "JSON",
				success: function (data) {
					if (data.status) {
						toastr.success('Data Sukses Tersimpan');
						$('#mod-add').modal('hide');
						$('#user').bootstrapTable('refresh');
						$("form-add").trigger('reset');
					} else {
						toastr.error('Data Gagal Tersimpan')
					}
					$('#btnSave').text('save');
					$('#btnSave').attr('disabled', false);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error adding / update data');
					$('#btnSave').text('save');
					$('#btnSave').attr('disabled', false);
				}
			});
			})
		});

		function edit(id){
			$.ajax({
			url: SITE_URL + 'backend/get_user/' + id,
			type: 'get',
			success: function (res) {
				$('.modal-title').text('Edit Detail');
				$('#mod-edit').modal('show');
				$('#id_user').val(res.id_user);
				$('#nama').val(res.nama);
				$('#username').val(res.username);
				$('#role').val(res.id_role).change();;
			}
		});
		}
		$('#form-edit').submit(function (event) {
			event.preventDefault();
			$('#btnEdit').text('saving...');
			$('#btnEdit').attr('disabled', true);
			bootbox.confirm("Are you sure?", function (result) {
			$.ajax({
				url: SITE_URL + 'backend/update',
				type: "POST",
				data: $('#form-edit').serialize(),
				dataType: "JSON",
				success: function (data) {
					if (data.status) {
						toastr.success('Data Sukses Tersimpan');
						$('#mod-edit').modal('hide');
						$('#user').bootstrapTable('refresh');
						$("form-edit").trigger('reset');
					} else {
						toastr.error('Data Gagal Tersimpan')
					}
					$('#btnEdit').text('Update');
					$('#btnEdit').attr('disabled', false);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error adding / update data');
					$('#btnEdit').text('save');
					$('#btnEdit').attr('disabled', false);
				}
			});
			})
		});

	</script>
</footer>

</html>
