<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Orm\User;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Builder;

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct(); 
		$this->load->model('Usermodel','user');
    }
	
	public function index()
	{
		$data['user']    = User::all();
		$data['user_ci'] = $this->db->get('user')->result();
		$data['user_role'] = $this->db->get('role')->result();
		$data['user_with_role'] = $this->user->Get_user_role();

		
		$this->load->view('rectest',$data);
	}
}
