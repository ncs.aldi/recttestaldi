<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Orm\User;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Builder;

class Backend extends CI_Controller {

	public function index()
	{
		$data = [
			'role' =>$this->db->get('role')->result(),
		];
		$this->load->view('backend',$data);
	}

	function list_user(){
		$limit=isset($_GET['limit']) ? $_GET['limit'] : 10;
		$offset=isset($_GET['offset']) ? $_GET['offset'] : 0;
		$search=(isset($_GET['search'])) ? $_GET['search'] : '';
		$sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'user_id';
		$order = (isset($_GET['order'])) ? $_GET['order'] : 'asc';

		$this->db->select('user.id_user,user.nama,role.nama_role,user.created_at');
        $this->db->from('user'); 
        $this->db->join('user_role', 'user_role.id_user=user.id_user', 'left');
        $this->db->join('role', 'role.id_role=user_role.id_role', 'left');
        $this->db->where('user.deleted_at', null);
		$this->db->limit($limit, $offset); 
        // $query = $this->db->get_compiled_select();
		if(!empty($search)){
			$this->db->like('user.nama', $search);
		}
        $row = $this->db->get()->result_array();$this->db->get('user')->result();
		$ret = [
			'total' => $this->db->get('user_role')->num_rows(),
			'rows' => $row
		];
		header('Content-Type: application/json');			
		echo json_encode($ret);	
       
	}

	function add_new(){
		$nama = $this->input->post('nama');
		$role = $this->input->post('role');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		// $password = password_hash($password, PASSWORD_DEFAULT); 
		$password = md5($password);

		$data_user = [
			'username' => $username,
			'nama' => $nama,
			'password' => $password,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		$this->db->insert('user',$data_user);
		$insert_id = $this->db->insert_id();

		$data_role = [
			'id_user' => $insert_id,
			'id_role' => $role,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];
		$user = $this->db->insert('user_role',$data_role);

		$ret['status'] = ($user >= 1) ? true : false;
		
		header('Content-Type: application/json');			
		echo json_encode($ret);	
	}

	function get_user($id){
		
		$this->db->select('user.id_user,user.nama,user.username,role.nama_role,user.created_at,user_role.id_role');
        $this->db->from('user'); 
        $this->db->join('user_role', 'user_role.id_user=user.id_user', 'left');
        $this->db->join('role', 'role.id_role=user_role.id_role', 'left');
        $this->db->where('user.id_user', $id);
        $row = $this->db->get()->row();
		header('Content-Type: application/json');			
		echo json_encode($row);	
	}

	function update(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$role = $this->input->post('role');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		// $password = password_hash($password, PASSWORD_DEFAULT); 
		$password = md5($password);

		$data_user = [
			'username' => $username,
			'nama' => $nama,
			'password' => $password,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];
		$this->db->where('id_user', $id);
		$this->db->update('user',$data_user);

		$data_role = [
			'id_user' => $id,
			'id_role' => $role,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];
		$this->db->where('id_user', $id);
		$user = $this->db->update('user_role',$data_role);

		$ret['status'] = ($user >= 1) ? true : false;
		
		header('Content-Type: application/json');			
		echo json_encode($ret);	
	}

	function del_user($id){
		//hard delete
		// $this->db->update('user', array('id_user' => $id));
		// $this->db->delete('user_role', array('id_user' => $id));

		//soft delete
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id_user', $id);
		$this->db->update('user');

		header('Content-Type: application/json');			
		echo json_encode(['status' => true]);	
	}
}
